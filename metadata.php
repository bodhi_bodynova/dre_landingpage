<?php
/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

$aModule = [
    'id'          => 'dre_landingpage',
    'title'       => [
        'en' => '<img src="../modules/bender/dre_landingpage/out/img/favicon.ico" title="Bodynova Landingpage Modul">odynova Landingpage Modul',
        'de' => '<img src="../moduels/bender/dre_landingpage/out/img/favicon.ico" title="Bodynova Landingpage Modul">odynova Landingpage',
    ],
    'description' => [
        'en' => 'Landingpage scripts',
        'de' => 'Landingpage Modul erstellt einfache Landing Pages',
    ],
    'thumbnail'   => 'out/img/logo_bodynova.png',
    'version'     => '2.0',
    'author'      => 'Bodynova GmbH',
    'url'         => 'https://bodynova.de',
    'email'       => 'support@bodynova.de',
    'controllers'       => [
        'dre_landingpage' => \Bender\dre_LandingPage\Application\Controller\dre_landingpage::class,
    ],
    'extend'      => [
    ],
    'templates'   => [
        'dre_landingpage.tpl' => 'bender/dre_landingpage/Application/views/tpl/dre_landingpage.tpl'
    ],
    'blocks'      => [
        array(
            'template' => 'actions_main.tpl',
            'block' => 'admin_actions_main_form',
            'file' => 'Application/views/admin/blocks/actions_main__admin_actions_main_form.tpl',
        ),
    ],
    'settings'    => [],
];